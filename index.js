//const createError = require('http-errors');
const express = require('express');
//const path = require('path');
const session = require('express-session');
const mongoose = require('mongoose');
var cors = require('cors');
//const http = require('http');
const user = require('./routes/users');
const bodyParser = require('body-parser');
const product = require('./routes/products');
//const post = require('./models/post');
//const post = require('./models/user');
const Comment = require('./models/comment');
//const Order = require('./models/order');
const commentRoute = require('./routes/comments');
const orderRoute = require('./routes/orders');


const configureServer = require('./server');
const configureRoutes = require('./routes');



const app = express();

mongoose.connect('mongodb://localhost:27017/ecommerce')
.then(() => { console.log("Connect to Database"); })
.catch( err => { console.log("Connection Failed!!"); });

var port = process.env.PORT || 5000;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.listen(port, () => { console.log("Connected to port "+port); });


app.use('/user', user);
app.use('/products', product);
app.use('/orders', orderRoute);
app.use('/comments', commentRoute);



//const SERVER_CONFIGS = require('./constants/server');





configureServer(app);
configureRoutes(app);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({ error: err });
});
/*
//app.use((err, req, res, next) => {
  // This check makes sure this is a JSON parsing issue, but it might be
  // coming from any middleware, not just body-parser:

  if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
      console.error(err);
      return res.sendStatus(400); // Bad request
  }

  next();
});
*/

module.exports = app;
