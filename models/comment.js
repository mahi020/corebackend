const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({

  msg: {type: String},
  userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  productId: {type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true},
  rating: { type: Number, required:true },
  date: { type: Date, default: Date.now() }

});

module.exports = mongoose.model('Comment', commentSchema);
