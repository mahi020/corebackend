const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const orderSchema = new Schema({

  
  userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  orderList: [{ productId: {type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true }}],
  qty: [{type: Number, required: true}],
  total: { type: Number, required: true },
  time: { type: Date, default: Date.now(), required: true },
  pending: { type: Boolean, required: true, default: true},
  delivered: { type: Boolean, required: true, default: false},
  received: { type: Boolean, required: true, default: false},
  address: { type: String },
  country: { type: String },
  zip: { type: String }

});

module.exports = mongoose.model('Order', orderSchema);
