const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const productSchema = new Schema({

  title: {type: String, required: true},
  image: {type: String, required: true},
  imageId: {type: String},
  desc: {type: String, required: true},
  price: {type: Number, required: true},
  category: {type: String, required: true},
  approved: {type: Boolean, required: true, default: false},
  author: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  rating: {type: Number, required: true, default: 0, min:0, max:5},
  ratingArrayId: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  commentArrayId: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  ratingArray: { type : Array , "default" : [] },
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],
  counter: {type: Number, required: true, default: 0},
  quantity: { type: Number, required: true, default: 0}
});

productSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Product', productSchema);
