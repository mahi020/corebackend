const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({

  firstname: {type: String, required: true},
  lastname: {type: String, required: true},
  email: {type: String, required: true},
  password: {type: String, required: true},
  is_admin: {type: Boolean , required: true, default: false},
  is_moderator: {type: Boolean , required: true, default: false},
  confirmed: {type: Boolean, required:true, default: false},
  productId: {type: mongoose.Schema.Types.ObjectId , ref: 'Product'},
  orders: {type: mongoose.Schema.Types.ObjectId , ref: 'Order'},
  token: { type: String },
  address: { type: String, required: true },
  country: { type: String, required: true },
  zip: { type: String, required: true }
  
});

module.exports = mongoose.model('User', userSchema);
