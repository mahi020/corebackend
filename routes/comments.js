const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Comment = require('../models/comment');
//const Comment = require('../models/comment');


router.get('/',function(req, res){

    Comment.find({}).then(result=>{

        res.status(200).json(result);

    }).catch(err=>{

        res.status(404).json({"msg": "Not Found!!"})

    });

});

router.post('/', function(req, res){

        const newComment = new Comment({

            msg: req.body.msg,
            userId: req.body.userId,
            productId: req.body.productId,
            rating: req.body.rating

        });
        newComment.save().then(data => {
            res.json(data);
          }).catch(err=>{
            console.log(err);
          });

});




module.exports = router;