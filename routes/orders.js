const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Order = require('../models/order');
const Product = require('../models/product');
const User = require('../models/user');


router.get('/',function(req, res){

    Order.find({}).populate('userId','firstname').populate('orderList.productId','title').then(result=>{

        res.status(200).json(result);

    }).catch(err=>{

        res.status(404).json({"msg": "Not Found!!"})

    });

});

router.post('/userOrder',function(req, res){

    //console.log(req.body.id);
    Order.find({userId:req.body.id}).populate('orderList.productId','title').then(result=>{

        res.status(200).json(result);

    }).catch(err=>{

        res.status(404).json({"msg": "Not Found!!"})

    });

});

router.post('/',function(req, res){

    //console.log(req.body);
    const { cart } = req.body;
    const { userId } = req.body;
    const { address } = req.body;
    const { country } = req.body;
    const { zip } = req.body;
    let { total } = req.body;
    total = total/100;
    console.log(req.body);
    const q = [];
    const ID = [];

    //const total=0;
    cart.map(data=>{
        q.push(data.qty);
        //console.log(data.qty);
        ID.push({productId: data._id});
        Product.updateOne({_id: data._id}, { $inc: { quantity: -data.qty, counter: data.qty}}).then(doc=>{
            return data;
        }).catch(err=>{
            console.log("Error "+err);
        });
        //total += data.total;
    });
    //console.log(cart);
    //console.log(q);
    //console.log(ID);
    //console.log(cart);
    //console.log(total);
    //console.log(userId);
    
    const newOrder = new Order({
  
        userId: userId,
        orderList: ID,
        total: total,
        qty: q,
        address,
        country,
        zip
          
    });
    //console.log("Lol");
    newOrder.save().then(data => {
        //console.log("Done");
        res.json(data);
      }).catch(err=>{
        //console.log("Not Done!!");
        res.status(404).json(err);
      });
    
  

});

router.post('/togglePendingStatus', function(req, res){

    Order.findOne({_id: req.body.id}).then(data=>{

        data.pending = !data.pending;
        data.delivered = !data.delivered;
        data.save().then(d=>{
            console.log("Saved Pending");
            return res.status(200).json(d);
        }).catch(e=>{
            console.log("Error Pending");
            console.log(e);
        });

    }).catch(err=>{
        console.log(err);
    });

});

router.post('/toggleDeliveryStatus', function(req, res){

   // console.log(req.body);

   Order.findOne({_id: req.body.id}).then(data=>{

    data.delivered = !data.delivered;
    data.pending = !data.pending;
    data.save().then(d=>{
        console.log("Saved Delivered");
        return res.status(200).json(d);
    }).catch(e=>{
        console.log("Error Delivered");
        console.log(e);
    });

}).catch(err=>{
    console.log(err);
});

});

router.post('/toggleReceivedStatus', function(req, res){

    //console.log(req.body);
    Order.findOne({_id: req.body.id}).then(data=>{

        data.received = !data.received;
        data.save().then(d=>{
            console.log("Saved Received");
            return res.status(200).json(d);
        }).catch(e=>{
            console.log("Error Received");
            console.log(e);
        });
    
    }).catch(err=>{
        console.log(err);
    });
    

});

module.exports = router;