const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
//const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');
const Comment = require('../models/comment');
//const Comment = require('../models/comment');
//const Order = require('../models/Order');
const multer = require('multer');
const cloudinary = require('cloudinary');


router.post('/', function(req, res){

    let numOfProd;
    //console.log(req.body.newPage);
    Product.countDocuments({$and: [{quantity: { $gt: 0 }},{ approved: true }] } ).then(r=>{
        numOfProd = r;
        //console.log("Culprit is Here");
        //console.log(numOfProd);
        let pageOption = 6;
        let page = req.body.newPage;
        page = page - 1;
        Product.paginate({$and: [{quantity: { $gt: 0 }},{ approved: true }] }, { offset: page*pageOption, limit:6 }).then(function(result) {
            
            //console.log(result.docs);
            res.status(200).json({
                data: result.docs,
                numOfProd: numOfProd,
                activePage: page+1
            });
        
        });
    
    }).catch(err=>{
        if(err)
        {
            //console.log("I'm not here");
            //console.error(err);
            res.status(404).json({'msg': 'Not Found!!'});
        }               
    });
    //console.log("I'm here Again");
    //console.log(c);
    //console.log(pageOptions);

    
});

router.get('/', function(req, res){

    Product.find({}).select('title quantity desc image price category approved').sort({ _id: -1 }).then(result=>{

        res.status(200).json(result);

    }).catch(err=>{

        res.status(404).json({"msg": "No Products Found!!"});

    });
    
});



router.post('/dropdown', function(req, res){

    let numOfProd;
    //console.log(req.body.newPage);
    Product.countDocuments({$and: [{quantity: { $gt: 0 }},{ category: req.body.category }, { approved: true }] }).then(r=>{
        numOfProd = r;
        //console.log("Culprit is Here");
        //console.log(numOfProd);
        let pageOption = 6;
        let page = req.body.newPage;
        page = page - 1;
        Product.paginate({$and: [{quantity: { $gt: 0 }},{ category: req.body.category }, { approved: true }] }, { offset: page*pageOption, limit:6 }).then(function(result) {
            
            //console.log(result.docs);
            res.status(200).json({
                data: result.docs,
                numOfProd: numOfProd,
                activePage: page+1
            });
        
        });
    
    }).catch(err=>{
        if(err)
        {
            //console.log("I'm not here");
            //console.error(err);
            res.status(404).json({'msg': 'Not Found!!'});
        }               
    });
    //console.log("I'm here Again");
    //console.log(c);
    //console.log(pageOptions);

    
});

router.post('/search',function(req, res){
   
    //console.log(req.body.search);
    let numOfProd;
    //console.log(req.body.newPage);
    let search = req.body.search;
    Product.countDocuments({$and: [{quantity: { $gt: 0 }}, { approved: true },{ title: new RegExp(search,'i') }] }).then(r=>{
        //console.log(r);
        numOfProd = r;
        //console.log("Culprit is Here");
        //console.log(numOfProd);
        let pageOption = 6;
        let page = req.body.newPage;
        page = page - 1;
        Product.paginate({$and: [{quantity: { $gt: 0 }}, { approved: true },{ title: new RegExp(search,'i') }] }, { offset: page*pageOption, limit:6 }).then(function(result) {
            //console.log(result);
            //console.log(result.docs);
            res.status(200).json({
                data: result.docs,
                numOfProd: numOfProd,
                activePage: page+1
            });
        
        });
    
    }).catch(err=>{
        if(err)
        {
            //console.log("I'm not here");
            //console.error(err);
            res.status(404).json({'msg': 'Not Found!!'});
        }               
    });
    //console.log("I'm here Again");
    //console.log(c);
    //console.log(pageOptions);

    

});



const storage = multer.diskStorage({
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
});

var imageFilter = function (req, file, cb) {

    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

const upload = multer({ storage: storage, fileFilter: imageFilter})


cloudinary.config({ 
  cloud_name: 'mahi020', 
  api_key: 424253614161897, 
  api_secret: 'VJt2AZR4AaZhyM-339Mz7X9pO4I'
});


router.post('/add', upload.single('image'), function(req, res) {

    console.log(req.body);
    //console.log("Hello 1");
    cloudinary.v2.uploader.upload(req.file.path, function(error,result) {

        //console.log('Hello 2');
        console.log(result);
        const newProduct = new Product({

            title: req.body.title,
            image: result.secure_url,
            imageId: result.public_id,
            desc: req.body.desc,
            price: req.body.price,
            category: req.body.category,
            author: req.body.author,
            quantity: req.body.quantity

        }); 
        //console.log('Hello 3');
        newProduct.save().then(data=>{
            //console.log('Hello 4');
            res.status(200).json(data);
        }).catch(err=> {
            console.log('Hello Error');
            return res.status(400).json({"msg": "Unsuccessful"});
        });
      });
    
});
router.get('/edit/:id',  function(req, res) {

        let id = req.params.id;
        console.log(id);
        Product.findOne({_id: id}).select('price quantity').then(data=>{
                res.status(200).json(data);
        }).catch(err=>{

            return res.status(404).json({"msg": "Not Found!!"});

        });
});

router.post('/update/:id',  function(req, res) {

    let id = req.params.id;
    console.log(id);
    Product.findOne({_id: id}).then(data=>{
            Product.updateOne({_id: id}, {$set: {'price': req.body.price, 'quantity': req.body.quantity}}).then(doc=>{
                res.status(200).json(doc);
            }).catch(error=>{
                res.status(400).json({"msg": "Unsuccessful Update!!"});
            });
    }).catch(err=>{

        return res.status(404).json({"msg": "Not Found!!"});

    });
});

router.get('/showSingleProduct', function(req, res){

    //console.log("Looool");
    //console.log(req.params);
    //console.log("Something has come "+req.params.id);
    
    let f=true;

    let newObj;
    let canRate = false;
    let canUpdate = false;
    let isSeller = false;
    let comments=[];

    
    Product.findOne({_id:req.query.productId}).then(result=>{
        
        Comment.countDocuments({productId: req.query.productId}, function(errr, cnt){

            if(errr)
            {
                console.log('1');
                res.status(401).json({"msg": "Error!!"});
            }
            
            console.log('2');
            Comment.find({productId: req.query.productId}).populate('userId','firstname lastname').exec(function(err, resss){
    
                    if(err)
                    {
                        //return res.status(404).json(err);
                        console.log(err);    
                    }
                    comments = resss;
                    
                    if(result.author == req.query.userId)
                    {
                        isSeller = true;
                    }

                if(result.ratingArrayId.length>0)
                {
                    console.log('3');
                    result.ratingArrayId.map(doc=>{
                    if(doc == req.query.userId)
                    {
                        f=false;
                        canUpdate = true;
                    }
                });
                }
                if(result.commentArrayId.length>0)
                {
                    
                    result.commentArrayId.map(doc=>{
                    if(doc == req.query.userId)
                    {
                        console.log('4');
                        f=false;
                        canUpdate = true;
                    }
                    });
                }
    
                if(req.query.userId && f)
                {
                    //console.log('5');
            
                 Order.countDocuments({userId: req.query.userId}, function(ee, count){

                    if(count>0)
                    {
                        console.log('6');
                        
                        Order.findOne({userId: req.query.userId}).then(r=>{

                        if(r.orderList.length>0)
                        {
                            //console.log('7');
                            r.orderList.map(d=>{
                                //console.log(d.productId);
                                if(d.productId == req.query.productId)
                                { 
                                    canRate = true;
                                    //console.log(req.query.productId);    
                                }
                            });
                        }
                        //console.log('Comments');
                        //console.log(comments)
                        newObj = {
                            data: result,
                            canRate: canRate,
                            canUpdate: canUpdate,
                            isSeller: isSeller,
                            comments: comments
                        }
                        //console.log("In Else");
                        //console.log(newObj);
                        res.status(200).json(newObj);
                
                    }).catch(e=>{
            
                        console.log(e);
                        
                    });
                }
                else
                {
                    console.log('8');
                    newObj = {
                        
                            data: result,
                            canRate: canRate,
                            canUpdate: canUpdate,
                            isSeller: isSeller,
                            comments: comments

                    }
                    //console.log("In Else...");
                    //console.log(newObj);
                    res.status(200).json(newObj);        
                }

            });
            
        }
        else
        {
            console.log('9');
            newObj = {
    
                data: result,
                canRate: canRate,
                canUpdate: canUpdate,
                isSeller: isSeller,
                comments: comments

            }
            console.log(newObj);
            //console.log("In Else...");
            //console.log(newObj);
            res.status(200).json(newObj);
    
        }

        });

        });
        

        

    }).catch(err=>{
        res.status(404).json({"msg": "Not Found!!"});
    });

   //console.log(req.query);

});
router.post('/review/:id', function(req, res){

    let id = req.params.id;
    console.log(req.body);
    let { rating, userId, comment } = req.body;
    let f=true;
    let canRate = false;
    //console.log(rating);
    //console.log(userId);
    //console.log(id);
    //console.log(req.body);
    console.log(comment);
    Product.findById(id).then(data=>{
        //console.log("Inside FindByID");
        if(data.ratingArrayId.length>0)
        {   
           //console.log("Inside First If 1");
            data.ratingArrayId.map(doc=>{
                //console.log("Inside Map");
                if(doc==userId)
                {
                    f=false;
                    //console.log("Nested if 2");
                }
            });
        }
        if(f)
        {
            //console.log("second if 3");
            data.ratingArrayId.push(userId);
            data.ratingArray.push(rating);
            data.commentArrayId.push(userId);
            //data.ratingArray
            let sum = data.ratingArray.reduce((previous, current) => current += previous);
            let avg = sum / data.ratingArray.length;
            //console.log(data.ratingArray);
            console.log(avg);
            data.rating = avg;
            canRate = false;

            data.save().then(d=>{
                //console.log("Inside Save 4");
                let newComment = new Comment({
                    msg: comment,
                    userId: userId,
                    productId: id,
                    rating: rating

                });
                newComment.save().then(dd=>{

                    let newObj = {
                        canRate: canRate
                    };
                    res.status(200).json(newObj);
        

                }).catch(ee=>{

                    console.log("Errror "+ee);

                });
                
            }).catch(e=>{
                console.log("Not Saved 5 "+e);
    
            });
    
        }
        
    }).catch(err=>{

        console.log(err);

    });

});

router.get('/review', function(req, res){

    //console.log(req.query);
    Comment.find({ userId: req.query.userId }).then(result=>{
 

        result.map(doc=>{

            if(doc.productId==req.query.productId)
            {
                let newObj = {
                    message: doc.msg,
                    rating: doc.rating
                }
                res.status(200).json(newObj);
            }

        });
        console.log(result);
        


    }).catch(err=>{

        console.log("Error "+err);
    });

});


router.post('/updatedReview', function(req, res){

    Comment.find({ userId: req.body.userId }).then(result=>{
 
        //console.log("In Find Comment 1");
        result.map(doc=>{

            if(doc.productId==req.body.productId)
            {
               //console.log("In Find Comment 1 Map");
                doc.msg = req.body.newComment;
                doc.rating = req.body.newRating;
                doc.date = Date.now();
                doc.save().then(d=>{
                

                Product.findOne({_id: req.body.productId}).then(rr=>{

                    let id = req.body.userId;
                    //console.log("In Find Product 1")
                    //console.log(rr.ratingArrayId);
                    rr.ratingArrayId.map((ddoc,index)=>{

                        //console.log("Inside Map");
                       // console.log(ddoc,index);
                        if(ddoc==id)
                        {
                            rr.ratingArray[index] = req.body.newRating;
                            let sum = rr.ratingArray.reduce((previous, current) => current += previous);
                            let avg = sum / rr.ratingArray.length;
                            //console.log(data.ratingArray);
                            console.log(avg);
                            rr.rating = avg;
                            rr.save().then(rrr=>{
                            res.status(200).json(rrr);
                            }).catch(eee=>{
                                console.log(eee);
                            });
            
                        }

                    });


                }).catch(ee=>{

                    console.log(ee);

                });

                
                }).catch(e=>{
                    console.log(e);
                });
            }

        });
        console.log(result);
        


    }).catch(err=>{

        console.log("Error "+err);
    });

});

router.post('/removeReview', function(req, res){

    Comment.find({ userId: req.body.userId }).then(data=>{

        data.map(doc=>{

            if(doc.productId==req.body.productId)
            {
                doc.remove().exec(function(err, d){

                    if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        console.log(d);
                    }

                });
            }
        });

    }).catch(e=>{

        console.log(e);

    });

    Product.findOne({_id: req.body.productId}).then(result=>{

            let index = result.ratingArrayId.indexOf(req.body.userId);
            if(index>-1)
            {
                result.ratingArrayId.splice(index, 1);
                result.commentArrayId.splice(index, 1);
                result.ratingArray.splice(index,1);
                if(result.ratingArray.length>0)
                {
                    let sum = result.ratingArray.reduce((previous, current) => current += previous);
                    let avg = sum / result.ratingArray.length;
                    //console.log(data.ratingArray);
                    console.log(avg);
                    result.rating = avg;
                }
                else
                {
                    result.rating = 0;
                }
                
            }

            result.save().then(data=>{

                let obj = {
                    canRate:true
                };
                res.status(200).json(obj);

            }).catch(err=>{

                console.log(err);
            });


    }).catch(error=>{


    })

});

router.get('/showAProduct/:id',  function(req, res) {

    let id = req.params.id;
    //console.log(id);
    Product.findOne({_id: id}).select('title quantity desc image price category approved').then(data=>{
            res.status(200).json(data);
    }).catch(err=>{

        return res.status(404).json({"msg": "Not Found!!"});

    });
});

router.post('/approved',  function(req, res) {

    let id = req.body.id;
    console.log(id);
    Product.findOne({_id: id}).then(data=>{
        Product.updateOne({_id: id}, {$set: {'approved': true}}).then(doc=>{
            res.status(200).json(doc);
        }).catch(error=>{
            res.status(400).json({"msg": "Unsuccessful Update!!"});
        });
    }).catch(err=>{

    return res.status(404).json({"msg": "Not Found!!"});

});

});

router.post('/userProducts',  function(req, res) {

    let id = req.body.id;
    //console.log(id);
    Product.find({author: id}).then(data=>{
        
        res.status(200).json(data);

    }).catch(err=>{

    return res.status(404).json({"msg": "Not Found!!"});

});

});

router.post('/deleteAProduct',  function(req, res) {

    let id = req.body.id;
    //console.log(id);
    Product.findOneAndDelete({ _id:id }).then(data=>{
        
        res.status(200).json(data);

    }).catch(err=>{

    return res.status(404).json({"msg": "Not Found!!"});

    });

});


module.exports = router;