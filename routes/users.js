const express = require('express');
const router = express.Router();
//const mongoose = require('mongoose');
const User = require('../models/user');
//const Product = require('../models/product');
const bcrypt = require('bcryptjs');
//const multer = require('multer');
const jwt = require('jsonwebtoken');
const auth = require('../middleware/auth');
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');
const Validator = require('validator');
const isEmpty = require('../validation/isEmpty');
const tokenList = {};
var nodemailer = require("nodemailer");


var smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
      user: "ecosite49@gmail.com",
      pass: "ecosite12345"
  }
});

var rand,mailOptions,link,ee;

router.post('/register', function(req, res){

    const { errors, isValid } = validateRegisterInput(req.body);

    if (!isValid)
    {
        return res.status(400).json(errors);
    }
    User.findOne({email: req.body.email}).then(user => {
      if(user)
      {
        console.log(user);
        return res.status(400).json({email: "Email already Exists"});
      }
      else
      {
          rand=Math.random().toString(36).substring(7);
          const newUser = new User({

                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password,
                token: rand,
                address: req.body.address,
                country: req.body.country,
                zip: req.body.zip

          });

          bcrypt.genSalt(10, (err, salt)=>{
              bcrypt.hash(newUser.password, salt, (err, hash)=>{
                if(err) throw err;

                newUser.password = hash;
                newUser.save().then(user => {
                  //console.log("Line 54");
                  
                  //host=req.get('host');
                  //console.log(host);
                  link="http://localhost:3000/login/"+rand;

                  mailOptions={
                    to : user.email,
                    subject : "Please confirm your Email account",
                      html : "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>"
                  }

                  console.log(mailOptions);
                  smtpTransport.sendMail(mailOptions, function(error, response){
                          
                    if(error)
                    {
                      console.log(error);
                      res.status(401).json({"msg":"Error in Sending Email"});
                    }
                    else
                    {
                      console.log("Message sent: " + response.message);
                      res.status(200).json({"msg": "Message Sent To Your Email, Please Verify First!!"});
                    }
                  });

                }).catch(err=>{
                  console.log(err);
                });

              });
          });
      }
    });
});

router.post('/verify',function(req,res){
  
  //console.log("Req.body:");
  //console.log(req.body);
  //console.log("Lol");
  if(req.body.id===rand)
  {
    //console.log("Done");
    //console.log(req.body.id);
    User.update({token: req.body.id}, { $set: { confirmed: true } }).then(result=>{
      //console.log(result);
      //console.log("email is verified");
      res.status(200).json({ "Success": true });
      
    }).catch(err=>{
      res.status(404).json({ "Success": false });
    });
    
  }
  else
  {
    console.log("Email is not verified");
    res.status(200).json({ "Success": false });
  }
});




router.post('/login', function(req, res){

    const { errors, isValid } = validateLoginInput(req.body);

    if (!isValid)
    {
        return res.status(400).json(errors);
    }
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({ email }).then(user => {
    // Check for user
    if (!user) {
      errors.email = 'User not found';
      return res.status(404).json(errors);
    }
    if(user.confirmed===false)
    {
      errors.email = 'Please Complete Verification First';
      return res.status(400).json(errors);
    }
    

    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {

        const payload = { id: user.id, firstname: user.firstname, email: user.email, isAdmin: user.is_admin, isModerator: user.is_moderator, shipping: user.shipping };
        const token = jwt.sign(
          payload,
          'secret',
          { expiresIn: 900 }
        );
        const refreshToken = jwt.sign(
          payload,
          'refreshSecret',
          { expiresIn: 14400 }
        );
        const response = {
              "status": "Logged In",
              "token": token,
              "refreshToken": refreshToken
        };
        tokenList[refreshToken] = response;
        res.status(200).json(response);

      } else {
        errors.password = 'Password incorrect';
        return res.status(400).json(errors);
      }
    });
  });
});

router.post('/token', function(req, res){

    const email = req.body.email;
    const refreshToken = req.body.refreshToken;
    //console.log("In Token");
    User.findOne({ email }).then(user => {

      //console.log("Found in token");
      if (!user) {
        errors.email = 'User not found';
        return res.status(404).json(errors);
      }

      if(refreshToken && refreshToken in tokenList)
      {
        //console.log("Refresh token Matched");
        const payload = { id: user.id, firstname: user.firstname, email: user.email , isAdmin: user.is_admin, isModerator: user.is_moderator};
        const token = jwt.sign(
          payload,
          'secret',
          { expiresIn: 900 }
        );
        const response = {
              "status": "Logged In",
              "token": token,
              "refreshToken": refreshToken
        };

        tokenList[refreshToken] = response;
        res.status(200).json(response);

      }
      else {
          //console.log("Token Error");
          res.status(404).json({"msg": "Invalid Request !!"});
      }

  });

});

router.get('/me/:id', function(req, res){

      //console.log(req.params);
      const user = User.findById(req.params.id).select('-password').then(doc =>{
        //console.log(doc);
        return res.status(200).json(doc);
      });

});

router.get('/anonymous', function(req, res){

  let payload = { name: "Anonymous"};
  let token = jwt.sign(
    payload,
    'secret'
  );
  let response = {

    "status": "Not Logged In",
    "token": token

  };
  res.status(200).json(response);

});

router.get('/', function(req, res){

  User.find({}).select('email firstname lastname confirmed is_moderator').then(result=>{

      res.status(200).json(result);

  }).catch(err=>{

      res.status(404).json({"msg": "No Users Found!!"});

  });
  
});

router.post('/makeModerator', function(req, res){

  let id = req.body.id;
  //console.log(id);
  User.findOne({_id: id}).then(data=>{
      User.updateOne({_id: id}, {$set: {is_moderator: true}}).then(doc=>{
          res.status(200).json(doc);
      }).catch(error=>{
          res.status(400).json({"msg": "Unsuccessful Update!!"});
      });
  }).catch(err=>{

  return res.status(404).json({"msg": "Not Found!!"});

});

router.post('/removeModerator', function(req, res){

  let id = req.body.id;
  //console.log(id);
  //console.log(id);
  User.findOne({_id: id}).then(data=>{
      User.updateOne({_id: id}, {$set: {is_moderator: false}}).then(doc=>{
          res.status(200).json(doc);
      }).catch(error=>{
          res.status(400).json({"msg": "Unsuccessful Update!!"});
      });
  }).catch(err=>{

        return res.status(404).json({"msg": "Not Found!!"});
  });

});

});

router.post('/findRecoveryEmail', function(req, res){

  let errors = {};

  //console.log(req.body);
  User.findOne({email: req.body.email}).then(user=>{

    if (!user) {
      errors.email = 'User not found';
      return res.status(404).json(errors);
    }
    rand=Math.random().toString(36).substring(7);

    mailOptions={
      to : req.body.email,
      subject : "Your Verification Code",
      html : "Your Verification Code is: "+rand
    }
  
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function(error, response){
            
      if(error)
      {
        console.log(error);
        res.status(401).json({"msg":"Error in Sending Email"});
      }
      else
      {
        //console.log("Message sent: " + response.message);
        let newObj = {

          e: false

        }
        res.status(200).json(newObj);
      }

    });
  


  }).catch(err=>{

    console.log(err);

  });
  
});

router.post('/verifyRecoveryCode',function(req, res){

  let errors = {};
  console.log(req.body);
  if(req.body.code==rand)
  {
    res.status(200).json({ pass: false });
  }
  else
  {
    errors.code = 'Verification Code Does not Match';
    return res.status(401).json(errors);
  }


});

router.post('/setNewPassword', function(req, res){

  console.log(req.body);
  let errors = {};
  let password = !isEmpty(req.body.newPassword) ? req.body.newPassword : '';
  
  if (Validator.isEmpty(password)) {
    errors.newPassword = 'Password field is required';
    return res.status(401).json(errors);
  }

  if (!Validator.isLength(password, { min: 6, max: 30 })) {
    errors.newPassword = 'Password must be at least 6 characters';
    return res.status(401).json(errors);
  }

  User.findOne({email: req.body.email}).then(user => {
    
        user.password = password;
        bcrypt.genSalt(10).then(salt=>{

          bcrypt.hash(user.password, salt).then(hash=>{

            user.password = hash;
              
            user.save().then(u => {
              console.log("Password Done");
              }).catch(errr=>{
            console.log(errr);
            });
              
        });


          }).catch(e=>{

            console.log(e);

          });

        }).catch(err=>{
            console.log(err);
        });
      
});

router.post('/shippingAddress',function(req, res){

  console.log(req.body);
  User.findOne({_id: req.body.id}).then(data=>{

    let newObj;
    console.log("lol1");
    //console.log(data.shipping);
    newObj = {
        address: data.address,
        country: data.country,
        zip: data.zip 
    };
    res.status(200).json(newObj);

  }).catch(err=>{

  });


});

module.exports = router;



