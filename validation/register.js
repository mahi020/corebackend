const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.firstname = !isEmpty(data.firstname) ? data.firstname : '';
  data.lastname = !isEmpty(data.lastname) ? data.lastname : '';
  data.address = !isEmpty(data.address) ? data.address : '';
  data.country = !isEmpty(data.country) ? data.country : '';
  data.zip = !isEmpty(data.zip) ? data.zip : '';



  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email field is required';
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Password must be at least 6 characters';
  }
  if (!Validator.isLength(data.firstname, { min: 2, max: 30 })) {
    errors.firstname = 'Firstname must be between 2 and 30 characters';
  }

  if (Validator.isEmpty(data.firstname)) {
    errors.firstname = 'Firstname field is required';
  }

  if (!Validator.isLength(data.lastname, { min: 2, max: 30 })) {
    errors.lastname = 'Lastname must be between 2 and 30 characters';
  }

  if (Validator.isEmpty(data.lastname)) {
    errors.lastname = 'Lastname field is required';
  }
  if (Validator.isEmpty(data.address)) {
    errors.address = 'Address field is required';
  }
  if (Validator.isEmpty(data.country)) {
    errors.country = 'Country field is required';
  }
  if (Validator.isEmpty(data.zip)) {
    errors.zip = 'Zip Code field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
